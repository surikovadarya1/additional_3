﻿#include <iostream>
struct T_list
{
    T_list* next;
    T_list* prev;
    int num;
};


void ADD(T_list* head, int num)
{
    T_list* temp = new T_list;
    T_list* p;
    p = head->next;

    head->next = temp;
    temp->num = num;
    temp->next = p;
    temp->prev = head;
    p->prev = temp;
}

void PRINT(T_list* head)
{
    T_list* p = head;
    do {
        p = p->next;
        std::cout << p->num << std::endl;
    } while (p != head);
}

void PRINTINV(T_list* head)
{
    T_list* p = head;
    do {
        std::cout << p->num << std::endl;
        p = p->prev;
    } while (p != head);
}

T_list* DELETEELEM(T_list* head, int k)
{
    T_list* p = nullptr;
    T_list* n = nullptr;
    T_list* tmp = head->prev;
    while ( p != tmp)
    {
        if (head->num == k)
        {
            p = head->prev;
            n = head->next;
            p->next = head->next;
            n->prev = head->prev;
            delete head;
            return p;
        }
        else
            head = head->next;
            p = head->prev;
    }
    throw std::exception("Number is not found.");
}


int main()
{
    T_list* head = new T_list;
    head->num = 0;
    head->next = head;
    head->prev = head;
    
    for (int i = 1; i < 5; i++)
    {
        ADD(head, i);
    }
    try
    {
        DELETEELEM(head, 1);
        DELETEELEM(head, 3);
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    PRINT(head);
}

